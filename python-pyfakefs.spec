%global _empty_manifest_terminate_build 0
Name:		python-pyfakefs
Version:	5.7.1
Release:	1
Summary:	pyfakefs implements a fake file system that mocks the Python file system modules.
License:	Apache-2.0
URL:		http://pyfakefs.org
Source0:	%{pypi_source pyfakefs}
BuildArch:	noarch

%description
pyfakefs implements a fake file system that mocks the Python file system moduls.
%package -n python3-pyfakefs
Summary:	pyfakefs implements a fake file system that mocks the Python file system modules.
Provides:	python-pyfakefs = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-pyfakefs
pyfakefs implements a fake file system that mocks the Python file system modules.

%package help
Summary:	Development documents and examples for pyfakefs
Provides:	python3-pyfakefs-doc
%description help
Development documents and examples for pyfakefs

%prep
%autosetup -n pyfakefs-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pyfakefs -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Nov 04 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.7.1-1
- Update package to version 5.7.1
  * Fixes a regression in version 5.7.0 that broke patching fcntl
  * adds official Python 3.13 support

* Thu Aug 29 2024 wangkai <13474090681@163.com> - 5.6.0-1
- Update package to version 5.6.0

* Tue Apr 23 2024 zhaojingyu <zhaojingyu@klinos.cn> - 5.4.1-1
- Update package to version 5.4.1
  fixed a regression from version 5.4.0 that incorrectly handled files opened twice via file descripto

* Tue Mar 05 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.3.5-1
- Update package to version 5.3.5
  Use cleanup hook to reload django views

* Mon Jul 31 2023 sunhui <sunhui@kylinos.cn> - 5.2.3-1
- Update package to version 5.2.3

* Thu May 04 2023 yaoxin <yao_xin001@hoperun.com> - 5.2.2-1
- Update to 5.2.2

* Mon Feb 27 2023 wubijie <wubijie@kylinos.cn> - 5.1.0-1
- Update package to version 5.1.0

* Tue Dec 06 2022 liukuo <liukuo@kylinos.cn> - 5.0.0-1
- Upgrade version to 5.0.0

* Tue Aug 9 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 4.6.3-1
- Update to 4.6.3

* Wed May 11 2022 yaoxin <yaoxin30@h-partners.com> - 4.4.0-2
- License compliance rectification

* Sun May 23 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
